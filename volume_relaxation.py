''' This script does volume relaxation using vasp.

Reads an input structure from the given file name
and then based on the input parameters, runs
volume relaxation.

The force cutoff for stopping the relaxation
is provided by fmax parameter. 

Snapshots of the relaxation is stored in file,
traj_<symbol>_vol_relaxed.traj which can be
viewed in ase-gui.

The relaxed structure is named as,
ase_<symbol>_vol_relaxed.vasp
'''

import numpy as np
import ase.io.vasp as vp
from ase.io import *
from ase.calculators.vasp import Vasp
from os import path, remove
from ase.optimize import BFGS as bfgs
from ase.constraints import UnitCellFilter as ucf
from ase.io.trajectory import Trajectory

np.set_printoptions(precision=2)

# name of input structure
ins_poscar='NbSe2_bulk_2H.vasp'


# force threshold on atoms
fmax = 1E-6

# maximum number of steps for the minimizer
steps = 150 

# converged kpoints for the structure
kpt=8

# minimizer log file
logfile = 'relax.log'

# vasp parameters
calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=0,
        sigma=0.1,
        lcharg=False,
        lwave=False,
        lreal = False,
        #isym=0,
        lorbit=11,
        #ispin=2,
        encut = 400,
        algo='Normal',
        nelm=200,
        ivdw=1,
        #ldau=True,
        #ldautype=2,
        #ldau_luj=ldau_param,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        kpts = [kpt,kpt,kpt/2],
        gamma=True)


print("Volume Relaxation using ASE")
print("---------------------------")

ins= vp.read_vasp(ins_poscar)


initial_pos = ins.get_positions()
initial_vol = ins.get_volume()
formula = ins.get_chemical_formula()

print('\n Initial volume: {:3.2f} A^3'.format(initial_vol))
print(' Initial Positions: ')

for p in initial_pos:
    print(' \t\t{}'.format(p))

traj_str = 'traj_{}_vol_relax.traj'.format(formula)

if path.isfile(logfile):
    remove(logfile)
    print('\n Cleared logfile ({}).'.format(logfile))


# Generate cell configurations
in2 = ins.copy()

in2.set_calculator(calc)

sf = ucf(in2)

bfgs = bfgs(sf, logfile=logfile)

traj = Trajectory(traj_str, 'w', in2)

bfgs.attach(traj)

print('\n Running optimizer...')

bfgs.run(fmax=fmax, steps=steps)

print('\n Optimizations done.')

label_str = 'volume relaxed {}'.format(formula)
poscar_name = 'ase_{}_vol_relaxed.vasp'.format(formula)

vp.write_vasp(poscar_name, in2, label=label_str,
              direct=False, sort=True, vasp5=True)

final_pos = in2.get_positions()
final_vol = in2.get_volume()

print('\n Final volume: {:3.2f} A^3'.format(final_vol))
print(' Final Positions: ')

for p in final_pos:
    print(' \t\t{}'.format(p))

diff_pos = final_pos - initial_pos

print('\n Change in volume: {:3.2f} A^3'.format(final_vol-initial_vol))
print(' Change in position: ')

for p in diff_pos:
    print(' \t\t{}'.format(p))

print('\n Stress on atoms in the final structure: ')

final_stress = calc.read_stress()

print('\t\t', end='')
for s in final_stress:
    print(' {:2.2E}'.format(s), end='')

#calc.clean()
       
print('\n\n \t----- Done -----\n')

