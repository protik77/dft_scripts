''' This script calculates band structure for 
a given structure. Uses converged charge density
from the SCF step for the calculation.

Reads an input structure from the directory name
given as variable isif_dir,
creates symlink of the CHGCAR file from `soc_scf_dir`
and based on the input parameters, does band structure
calculation.


The path for the band structure calculation
is given by `bs_str` assuming the Brillouin
zone is hexagonal. k-points per line is given
by `kp_per_line`. The path for the band structure
is created accordingly.

When the calculation is done, the output files
are stored in a directory name given in `soc_ek_dir`.

The files stored in the soc_ek_dir are:
vasprun.xml, OUTCAR and vasp.out.
'''

import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, makedirs, rename, remove,symlink
from ase.dft.kpoints import get_special_points, bandpath
import numpy as np
from shutil import *

kp_per_line=51

bs_str = 'GMKG'

soc_ek_dir='2.ek'

soc_scf_dir='1.scf'

isif_dir='0.isif'

calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=1,
        sigma=0.1,
        lwave=False,
        lcharg=False,
        lreal = False,
        icharg=11,
        lorbit=11,
        #nbands=70,
        encut = 550,
        algo='Normal',
        #ncore=12,
        #kpar=4,
        nelm=100,
        #isym=0,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        reciprocal=True,
        )

print("\n-------------------------------------------")
print("E-k calculation of Heterostructure using ASE")
print("--------------------------------------------")


poscar_path=path.join(isif_dir,'CONTCAR')

l_scf_dir= path.join(soc_scf_dir,'CHGCAR')

l_ek_dir=path.join(soc_ek_dir)

if path.islink('CHGCAR') or path.isfile('CHGCAR'):
    remove('CHGCAR')

symlink(l_scf_dir,'CHGCAR')

if not path.exists(l_ek_dir):
    makedirs(l_ek_dir)       

b=vp.read_vasp(poscar_path)

in2 = b.copy()

points = get_special_points('hexagonal', in2.cell)
GMK = [points[k] for k in bs_str]

no_kpts=kp_per_line*(len(GMK)-1)

temp_kpts, x, X = bandpath(GMK, in2.cell, no_kpts)

del x
del X

final_shape = temp_kpts.shape

kpts=np.zeros((final_shape[0],4))

wt = np.ones((no_kpts,1), dtype=np.float64)

kpts[:,:-1]=temp_kpts
kpts[:,-1:] = wt

del temp_kpts
del wt

calc.set(kpts=kpts,
    )

in2.set_calculator(calc)

energy = in2.get_potential_energy()

fpath2 ='{}/vasprun.xml'.format(l_ek_dir)

rename('vasprun.xml',fpath2)

fpath1 = '{}/vasp.out'.format(l_ek_dir)

copyfile('vasp.out',fpath1)

fpath4 ='{}/OUTCAR'.format(l_ek_dir)

rename('OUTCAR',fpath4)

#calc.clean()
  
print('\n\n \t----- E-k Done -----\n')

