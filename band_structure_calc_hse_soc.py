''' This script calculates band structure with SOC and HSE for 
a given structure. Uses converged charge density
from the SCF step with SOC for the calculation.

Reads an input structure from the directory name
given as variable `isif_dir`,
creates symlink of the `WAVECAR` file from `hse_scf_name`,
and based on the input parameters, does band structure
calculation.

The `k-points` for the irreducible Brillouin zone (IBZ)
is read from `hse_scf_name` directory and added
to the `KPOINTS` file.

The path for the band structure calculation
is given by `bs_str` assuming the Brillouin
zone is hexagonal. `k-points` per line is given
by `kp_per_line`. These `k-points` are appended
after the `k-points` of the IBZ.

When the calculation is done, the output files
are stored in a directory name given in `soc_ek_dir`.

The files stored in the `soc_ek_dir` are:
`vasprun.xml`, `OUTCAR` and `vasp.out`.
'''

import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, makedirs, rename, remove, symlink
from ase.dft.kpoints import get_special_points, bandpath
import subprocess
import numpy as np
from shutil import copyfile

kp_per_line=51

bs_str = 'GMKG'

# initial moment
mom=1

hse_ek_name='6.hse.ek'

hse_scf_name='5.hse.scf'

relax_name='0.isif'

calc = Vasp(prec = 'Accurate', 
        xc = 'hse06', 
        istart=1,
        ismear=0,
        sigma=0.1,
        lcharg=False,
        lwave=False,
        lreal = False,
        lorbit=11,
        algo='All',
        #encut = 400,
        kpar=4,
        #algo='All',
        nelm=150,
        ncore=24,
        #nelmdl=-8,
        nbands=50,
        isym=0,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        lsorbit=True,
        lhfcalc=True,
        hfscreen=0.2,
        time=0.4,
        precfock='Normal',
        aexx=0.25,
        lmaxmix=4,
        nsim=4,
        reciprocal=True)

print("\n----------------------------------------------")
print("HSE-Ek calculation of Heterostructure using ASE")
print("-----------------------------------------------")

hse_ek_dir=path.join(hse_ek_name)

hse_scf_dir=path.join(hse_scf_name)

relax_dir_adr=path.join(relax_name)

poscar_str= path.join(relax_dir_adr,'CONTCAR')

het = vp.read_vasp(poscar_str)

in2 = het.copy()

ibzkp_str=path.join(hse_scf_dir,'ibzkp.txt')

ibzkp_wt_str=path.join(hse_scf_dir,'ibzkp_wt.txt')

ibzkp=np.loadtxt(ibzkp_str)

ibzkp_wt=np.loadtxt(ibzkp_wt_str)

kpts1=np.zeros((len(ibzkp),4))

kpts1[:,:-1] = ibzkp
kpts1[:,-1:] = ibzkp_wt.reshape(len(ibzkp_wt),1)

del ibzkp
del ibzkp_wt

points = get_special_points('hexagonal', in2.cell)
GMK = [points[k] for k in bs_str]

no_kpts=kp_per_line*len(GMK)

temp_kpts, x, X = bandpath(GMK, in2.cell, no_kpts)

del x
del X

temp_shape = temp_kpts.shape

kpts2=np.zeros((temp_shape[0],4))

wt = np.zeros((no_kpts,1), dtype=np.float64)

kpts2[:,:-1]=temp_kpts
kpts2[:,-1:] = wt

kpts=np.vstack((kpts1,kpts2))

del kpts2
del temp_kpts
del wt

magmom=np.ones(len(in2)*3)*mom

calc.set(kpts=kpts,
        magmom=magmom,
        )

wcar_dir=path.join(hse_scf_dir,'WAVECAR')

if path.islink('WAVECAR'):
    remove('WAVECAR')

symlink(wcar_dir,'WAVECAR')

in2.set_calculator(calc)

energy = in2.get_potential_energy()

if not path.exists(hse_ek_dir):
    makedirs(hse_ek_dir)       

fpath1 = '{}/vasp.out'.format(hse_ek_dir)

rename('vasp.out',fpath1)

fpath4 ='{}/OUTCAR'.format(hse_ek_dir)

rename('OUTCAR',fpath4)

fpath4 ='{}/vasprun.xml'.format(hse_ek_dir)

rename('vasprun.xml',fpath4)

calc.clean()

print('\n\n \t----- Done -----\n')

