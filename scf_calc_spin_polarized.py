''' This script does spin polarized SCF calculation for 
a given structure.

Reads an input structure from the directory name
given as variable isif_dir,
and based on the input parameters, does SCF
calculation.

When the calculation is done, the output files
are stored in a directory name given as
scf_dir variable.

Optionally it can process the LOCPOT file
to generate data for local potential plot.

The files stored in the scf_dir are:
CHGCAR, OSZICAR, OUTCAR and vasp.out
and optionally WAVECAR file which is
required for HSE calculations.

In this particular case this script runs
spin polarized SCF calculation on AFM
phase of CrSb. The initial magnetic moment
is set manually.

Optionally LDA+U parameters
can also be incorporated.
'''

import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, makedirs, rename, remove
from shutil import *
import subprocess
import numpy as np

conv_kpt=[14, 14, 14]

scf_dir='1.sp.scf'

# initial moment
mom=3.5
def_mom=0
#u_val=0.25

#ldau_param={'Cr':{'L':2, 'U':u_val, 'J':0.0},
            #'Sb':{'L':-1, 'U':0.0, 'J':0.0},
           #}

isif_dir='0.isif'

calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=0,
        sigma=0.05,
        lwave=False,
        lreal = False,
        #lvtot=True,
        encut = 400,
        algo='Normal',
        #ncore=12,
        #kpar=6,
        ispin=2,
        #ldau=True,
        #ldautype=2,
        #ldau_luj=ldau_param,
        nelm=200,
        #isym=0,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        kpts = conv_kpt,
        gamma=True)

print("\n-------------------------------------------")
print("SCF calculation of Heterostructure using ASE")
print("--------------------------------------------")

poscar_path=path.join(isif_dir,'CONTCAR')

l_scf_dir= path.join(scf_dir)

if path.islink('CHGCAR') or path.isfile('CHGCAR'):
    remove('CHGCAR')

if not path.exists(l_scf_dir):
    makedirs(l_scf_dir)       

b=vp.read_vasp(poscar_path)

in2 = b.copy()

# set magnetic moments
magmom=np.ones(len(in2)) * def_mom
mn_ind=0
for idx, atom in enumerate(in2):

    if atom.symbol == 'Cr' and mn_ind==0 :
        magmom[idx] = mom
        mn_ind= mn_ind + 1
    elif atom.symbol == 'Cr' and mn_ind==1 :
        magmom[idx] = -mom
        break


calc.set(magmom=magmom,
    )

in2.set_calculator(calc)

energy = in2.get_potential_energy()

#locpot_path=path.join(l_scf_dir,'LOCPOT')

#move('LOCPOT',locpot_path)

#cmd=['vtotav',locpot_path,'Z']

#print('\n')

#subprocess.call(cmd)

fpath1 = '{}/vasp.out'.format(l_scf_dir)

copyfile('vasp.out',fpath1)

fpath3 ='{}/OSZICAR'.format(l_scf_dir)

rename('OSZICAR',fpath3)

fpath4 ='{}/OUTCAR'.format(l_scf_dir)

rename('OUTCAR',fpath4)

fpath5 ='{}/CHGCAR'.format(l_scf_dir)

rename('CHGCAR',fpath5)

calc.clean()

print('\n\n \t----- SCF Done -----\n')

