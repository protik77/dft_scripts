
Various scripts to pre- and post-process DFT calculations using `VASP`.
All the scripts are based on [ASE](https://wiki.fysik.dtu.dk/ase/) library.

## Table of contents

* [Example job submission script](#markdown-header-job-submission-script)
* [Convergence and relaxation](#markdown-header-convergence-and-relaxation)
    * [`KPOINTS` convergence](#markdown-header-kpoint_convergencepy)
    * [`ENCUT` convergence](#markdown-header-encut_convergencepy)
    * [Volume relaxation](#markdown-header-volume_relaxationpy)
    * [Volume relaxation with LDA+U](#markdown-header-volume_relaxation_ldaupy)
    * [Surface relaxation](#markdown-header-surface_relaxationpy)

* [Electronic calculations](#markdown-header-electronic-calculations)
    * [SCF calculation](#markdown-header-scf_calcpy)
    * [Band structure calculation](#markdown-header-band_structure_calcpy)
    * [Spin polarized SCF calculation](#markdown-header-scf_calc_spin_polarizedpy)
    * [Spin polarized band structure calculation](#markdown-header-band_structure_spin_polarizedpy)
    * [SCF calculation with SOC](#markdown-header-scf_calc_socpy)
    * [Band structure calculation with SOC](#markdown-header-band_structure_calc_socpy)
	* [DOS calculation](#markdown-header-dos_ldaupy)
* [Electronic calculations with HSE](#markdown-header-electronic-calculations-with-hse)
    * [SCF calculation with HSE](#markdown-header-scf_calc_socpy)
    * [Band structure calculation with HSE](#markdown-header-band_structure_calc_hse_socpy)
* [Nudged Elastic Band (NEB) calculations](#markdown-header-nudged-elastic-band-NEB-calculations )
    * [NEB calculation](#markdown-header-nudged_elastic_band_calcpy)
    * [Restart NEB calculation](#markdown-header-restart_nudged_elastic_band_calcpy)
* [Binding energy calculations](#markdown-header-binding energy-calculations)
    * [Surface relaxation of atomic adsorption](#markdown-header-binding-energy-calculations)
    * [Surface relaxation of hBN layer adsorption](#markdown-header-hbn_binding_energypy)


Job submission script
---------------------

All the scripts in this repo can be run using a slurm/PBS scheduler. 
Example of using a script in a slurm scheduler is,

```bash

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=24
#SBATCH -t 47:55:00
#SBATCH -o output.log 
#SBATCH -e error.log 
#SBATCH --job-name="job name"
#SBATCH -p compute

source activate ase

processors=$(( $SLURM_NNODES * $SLURM_NTASKS_PER_NODE ))

exe=`which com_vasp_std`
export VASP_COMMAND="mpirun -np $processors -genv I_MPI_FABRICS shm:ofa $exe"

echo "Starting job $SLURM_JOBID with $processors processors..."

python <script_name.py>
```

The following are the names of the scripts along with a brief summary.

Convergence and relaxation 
==========================


kpoint_convergence.py 
---------------------

Checks k-points convergence using vasp.

This script reads an input structure given as the variable `ins_poscar`,
runs k-points convergence for each of the
k-point given in the `kps` list and tabulates them
in a file name given by `out_summary`.

encut_convergence.py 
--------------------

Checks energy convergence using vasp.

This script reads an input structure given as the variable `ins_poscar`,
runs energy convergence for each of the
`encut` given in the `encuts` list and tabulates them
in a file name given by `out_summary`.

volume_relaxation.py
--------------------

Does volume relaxation using vasp.

Reads an input structure given as `ins_poscar`
and then based on the input parameters, runs
volume relaxation.

The force cutoff for stopping the relaxation
is provided by `fmax`. 

Snapshots of the relaxation is stored in file,
`traj_<symbol>_vol_relaxed.traj` which can be
viewed using `ase gui`.

The relaxed structure is saved as,
`ase_<symbol>_vol_relaxed.vasp`

volume_relaxation_ldau.py
-------------------------

Does volume relaxation using `vasp`.

Reads an input structure from variable `ins_poscar`
and then based on the input parameters, runs
volume relaxation with LDA+U.

Also the LDA+U parameter are provided from
`ldau_param` dictionary.

The force cutoff for stopping the relaxation
is provided by `fmax` parameter. 

Snapshots of the relaxation is stored in file,
`traj_<symbol>_vol_relaxed.traj` which can be
viewed using `ase gui`.

The relaxed structure is named as,
`ase_<symbol>_vol_relaxed.vasp`

surface_relaxation.py
---------------------

Does surface relaxation using `vasp`.

Reads an input structure from variable `ins_poscar`
and then based on the input parameters, runs
surface relaxation.

This script uses `ISIF=2` tag of `vasp` for the relaxation
as it turns out to be faster.

The relaxed structure is stored in the directory
name given in `isif_dir` variable and stored as 'CONTCAR'.
This file is read for further calculations.

The relaxed structure is named as 
`ase_<symbol>_vol_relaxed.vasp`.


Electronic calculations 
=======================

scf_calc.py
-----------

Does SCF calculation for a given
structure.

Reads an input structure from the directory name
given as variable `isif_dir`
and then based on the input parameters, does SCF
calculation.

When the calculation is done, the output files
are stored in a directory name given as
`scf_dir` variable.

The files stored in the scf_dir are:
`CHGCAR`, `OSZICAR`, `OUTCAR` and `vasp.out`.

band_structure_calc.py
----------------------

Calculates band structure for 
a given structure. Uses converged charge density
from the SCF step for the calculation.

Reads an input structure from the directory name
given as variable `isif_dir`,
creates symlink of the `CHGCAR` file from `soc_scf_dir`
and based on the input parameters, does band structure
calculation.


The path for the band structure calculation
is given by `bs_str` assuming the Brillouin
zone is hexagonal. `k-points` per line is given
by `kp_per_line`. The path for the band structure
is created accordingly.

When the calculation is done, the output files
are stored in a directory name given in `soc_ek_dir`.

The files stored in the `soc_ek_dir` are:
`vasprun.xml`, `OUTCAR` and `vasp.out`.

scf_calc_spin_polarized.py
--------------------------

Does spin polarized SCF calculation for 
a given structure.

Reads an input structure from the directory name
given as variable `isif_dir`,
and based on the input parameters, does SCF
calculation.

Optionally it can process the `LOCPOT` file
to generate data for local potential plot.

When the calculation is done, the output files
are stored in a directory name given as
`scf_dir` variable.

The files stored in the directory defined by
`scf_dir` are:
`CHGCAR`, `OSZICAR`, `OUTCAR` and `vasp.out`
and optionally `WAVECAR` file which is
required for HSE calculations.

In this particular case this script runs
spin polarized SCF calculation on AFM
phase of CrSb. The initial magnetic moment
is set manually.

Optionally LDA+U parameters
can also be incorporated.


band_structure_spin_polarized.py
--------------------------------

Calculates spin polarized band structure with LDA+U for 
a given structure. Uses converged charge density
from the SCF step with SOC for the calculation.

Reads an input structure from the directory name
given as variable `isif_dir`,
creates symlink of the `CHGCAR` file from `sp_scf_dir`
and based on the input parameters, does band structure
calculation.

The path for the band structure calculation
is given by `ekpath_str` assuming the Brillouin
zone is hexagonal. `k-points` per line is given
by `kp_per_line`. The path for the band structure
is created accordingly.

When the calculation is done, the output files
are stored in a directory name given in `sp_ek_dir`.

The files stored in the soc_ek_dir are:
`vasprun.xml`, `OUTCAR` and `vasp.out`.



scf_calc_soc.py
---------------

Does SCF calculation with SOC for 
a given structure. Reads the initial charge density
from the SCF calculation without SOC.

Reads an input structure from the directory name
given as variable `isif_dir`,
copies `CHGCAR` file from directory name given
as `scf_dir` variable
and based on the input parameters, does SCF
calculation using `vasp`.

When the calculation is done, the output files
are stored in a directory name given as
`scf_dir` variable.

Optionally it can process the `LOCPOT` file
to generate data for local potential plot.

The files stored in the `scf_dir` are:
`CHGCAR`, `OSZICAR`, `OUTCAR` and `vasp.out`
and optionally `WAVECAR` file which is
required for HSE calculations


band_structure_calc_soc.py
--------------------------

Calculates band structure with SOC for 
a given structure. Uses converged charge density
from the SCF step with SOC for the calculation.

Reads an input structure from the directory name
given as variable isif_dir,
creates symlink of the CHGCAR file from `soc_scf_dir`
and based on the input parameters, does band structure
calculation.

The path for the band structure calculation
is given by `bs_str` assuming the Brillouin
zone is hexagonal. k-points per line is given
by `kp_per_line`. The path for the band structure
is created accordingly.

When the calculation is done, the output files
are stored in a directory name given in `soc_ek_dir`.


The files stored in the soc_ek_dir are:
`vasprun.xml`, `OUTCAR` and `vasp.out`.


dos_ldau.py
-----------

Calculates DOS with LDA+U for 
a given structure. Uses converged charge density
from the SCF step with SOC for the calculation.

Reads an input structure from the directory name
given as variable `isif_dir`,
creates symlink of the `CHGCAR` file from `sp_scf_dir`
and based on the input parameters, does DOS
calculation.

When the calculation is done, the output files
are stored in a directory `dos<number_of_kpoints>`.

The script read the Fermi level from
the `OUTCAR` of SCF directory which is
given as `soc_scf_dir`. The DOS calculation is done
from `below_fermi` to `over_fermi` with the `resolution`
given in meV.

The files stored in the `dos<number_of_kpionts>` are:
`vasprun.xml`, `OUTCAR` and `vasp.out`.

Electronic calculations with HSE 
================================

scf_calc_soc.py
---------------

Does SCF calculation with SOC and 
HSE for a given structure. Reads the wavefunctions 
from the SCF calculation with SOC.

Reads an input structure from the directory name
given as variable `isif_dir`,
copies `WAVECAR` file from directory name given
as `soc_scf_dir` variable
and based on the input parameters, does SCF
calculation.

When the calculation is done, the output files
are stored in a directory name given as
`hse_scf_dir` variable. Also stores `k-points` and `weights`
of the irreducible Brillouin zone in 
`ibzkp.txt` and `ibzkp_wt.txt` files, respectively.
This is used later for band structure calculation with HSE.

Optionally it can process the `LOCPOT` file
to generate data for local potential plot.

The files stored in the `hse_scf_dir` are:
`CHGCAR`, `OUTCAR`, `vasp.out` and `WAVECAR`
file which is required for HSE band structure
calculations.

band_structure_calc_hse_soc.py
------------------------------

Calculates band structure with SOC and HSE for 
a given structure. Uses converged charge density
from the SCF step with SOC for the calculation.

Reads an input structure from the directory name
given as variable `isif_dir`,
creates symlink of the `WAVECAR` file from `hse_scf_name`,
and based on the input parameters, does band structure
calculation.

The `k-points` for the irreducible Brillouin zone (IBZ)
is read from `hse_scf_name` directory and added
to the `KPOINTS` file.

The path for the band structure calculation
is given by `bs_str` assuming the Brillouin
zone is hexagonal. `k-points` per line is given
by `kp_per_line`. These `k-points` are appended
after the `k-points` of the IBZ.

When the calculation is done, the output files
are stored in a directory name given in `soc_ek_dir`.

The files stored in the `soc_ek_dir` are:
`vasprun.xml`, `OUTCAR` and `vasp.out`.


Nudged Elastic Band (NEB) calculations 
======================================

nudged_elastic_band_calc.py
---------------------------

Does nudged elastic band (NEB) calculation using [VTST tools](http://theory.cm.utexas.edu/vtsttools/index.html) for vasp.

Reads the initial and final structure from `initial` and
`final`, creates `nimages` number of intermediate images
using `nebmake.pl` provided by VTST and runs
NEB calculation from the given input parameters.

Uses the default NEB module of vasp.

Also creates some dummy files to avoid errors.

restart_nudged_elastic_band_calc.py
-----------------------------------

Restarts nudged elastic band (NEB) calculation.

In case the NEB calculation is not converged,
this script restarts the calculation.

The `nimages` needs to be the same as the initial
NEB calculation.

Also creates some dummy files to avoid errors.

Binding energy calculations 
===========================

oct.binding.energy.py
---------------------

Surface relaxation of B/N atom
on different sites of a metal (111) surface (i.e. Cu/Co/Ni)

This script puts an interstitial C atom on the octahedral site close to
the 111 surface then relaxes the B/N atom on different sites.

The `sup_size` variable defines the supercell size and the
`z_layers` defines the number of layers. For example `sup_size`
and `z_layers` as 4 and 4 denotes a surface supercell of
size 4x4x4. 

`int_dist` is the distance between the adsorotion atom
and the top of the surface.

`int_c` is a boolean flag to define whether the interstitial
C atom will be there or not.

`add_bn` defines the symbol of the adsorption atom.

`run_flag` defines calculation for which site is to be run.

`sites` list defines the adsorption sites of a 111 surface.

The final energies are tabulated in a file defined by 
`out_summary`. Functions `check_convergence`, `calc_binding_energy`
and `get_distance` found in `utility_functions.py` can be used
to extract binding energy of the sites.

All the relaxed structure along with vasp.out and OUTCAR
files are copied into `data-dir` with file name
`<site_name>.<file_name>`

hbn_binding_energy.py
---------------------

Surface relaxation of a single hexagonal
boron nitride (hBN) layer
on different sites of a metal 111 surface (i.e. Cu/Co/Ni)

This script puts an interstitial C atom on the octahedral site close to
the 111 surface then relaxes the B/N atom on different sites.

The `sup_size` variable defines the supercell size and the
`z_layers` defines the number of layers. For example `sup_size`
and `z_layers` as 4 and 4 denotes a surface supercell of
size 4x4x4. 

The hBN layer is strained to the lattice constant of the metal
surface.

`int_dist` is the distance between the adsorotion atom
and the top of the surface.

`int_c` is a boolean flag to define whether the interstitial
C atom will be there or not.

`add_bn` defines the symbol of the adsorption atom.

`run_flag` defines calculation for which site is to be run.

`sites` list defines the adsorption sites of a 111 surface.

`restart` list defines the site for which the calculation
to be restarted. For this case the final structure is used
for initial relaxation.

The final energies are tabulated in a file defined by 
`out_summary`. Functions `check_convergence`, `calc_binding_energy`
and `get_distance` found in `utility_functions.py` can be used
to extract binding energy of the sites.


All the relaxed structure along with vasp.out and OUTCAR
files are copied into `data-dir` with file name
`<site_name>.<file_name>`




