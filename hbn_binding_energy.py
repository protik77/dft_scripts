''' This script does surface relaxation of a single hexagonal
boron nitride (hBN) layer
on different sites of a metal 111 surface (i.e. Cu/Co/Ni)

This script puts an interstitial C atom on the octahedral site close to
the 111 surface then relaxes the B/N atom on different sites.

The `sup_size` variable defines the supercell size and the
`z_layers` defines the number of layers. For example `sup_size`
and `z_layers` as 4 and 4 denotes a surface supercell of
size 4x4x4. 

The hBN layer is strained to the lattice constant of the metal
surface.

`int_dist` is the distance between the adsorotion atom
and the top of the surface.

`int_c` is a boolean flag to define whether the interstitial
C atom will be there or not.

`add_bn` defines the symbol of the adsorption atom.

`run_flag` defines calculation for which site is to be run.

`sites` list defines the adsorption sites of a 111 surface.

`restart` list defines the site for which the calculation
to be restarted. For this case the final structure is used
for initial relaxation.

The final energies are tabulated in a file defined by 
`out_summary`. Functions `check_convergence`, `calc_binding_energy`
and `get_distance` found in `utility_functions.py` can be used
to extract binding energy of the sites.


All the relaxed structure along with vasp.out and OUTCAR
files are copied into `data-dir` with file name
<site_name>.<file_name>
'''

from ase.build import fcc111,add_adsorbate
from ase import Atoms as at
import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from ase.constraints import FixAtoms, FixedPlane
from os import path, makedirs, rename, remove
import numpy as np

# input: how many layers of the surface in the z direction?
sup_size=4
z_layers=4
relax_n_layers=2     # how many layers to fix
m_ind=0              # index of the adsorbate atom to set
int_dist=3.1
int_c=True

sp_a = 3.52074 # lattice constant in Angstrom

conv_kpt=[2, 2, 1]

#run_flag=[1, 1, 1, 1, 1]
#run_flag=[0, 0, 0, 0, 1]
#run_flag=[0, 0, 0, 1, 1]
#run_flag=[0, 0, 0, 0, 1]
#run_flag=[1, 0, 0, 0, 0]
run_flag=[1, 1, 1, 1, 1]
sites=['co-surf','hbn','fcc','hcp','ontop']

restart = [0, 0, 0, 0, 0]

out_summary='bn_summary.txt'
data_dir='data'


print('\n------------------------------------------------------')
print('            Calculation of binding energy')
print('                Written by: Protik Das                ')
print('------------------------------------------------------\n\n')

if not path.exists(data_dir):
    makedirs(data_dir)

if not path.isfile(out_summary):
    fl=open(out_summary,'w')
    fl.write('{:10s} {:10s}\n'.format('Config', 'Energy (eV)'))
    fl.flush()
else:
    fl=open(out_summary,'a')


bn_poscar='BN_rotated.vasp'
top1 = vp.read_vasp(bn_poscar)
top = top1 * (sup_size,sup_size,1)

bot_111 = fcc111('Ni', size=(sup_size,sup_size,z_layers), a=sp_a, vacuum=15.0) # create surface with a n X n X n supercell

bot_cell = bot_111.get_cell()

top_cell = top.get_cell()

bot_cell[:,2]=top_cell[:,2]

top.set_cell(bot_cell, scale_atoms=True)

top.wrap()

print(' Sites {}'.format(sites))
print(' Run flag {}'.format(run_flag))


for ix, site in enumerate(sites):
    
    if run_flag[ix]==1:
        
        print('\n\n Running for \'{}\' site.\n'.format(site))
        
        calc = Vasp(prec = 'Accurate', 
            pp = 'pbe', 
            istart=0,
            ismear=1,
            sigma=0.05,
            lcharg=False,
            lwave=False,
            lreal = 'Auto', # DO NOT EDIT
            algo = 'Normal',
            encut = 550,
            ediff=1e-6,
            ibrion=1,
            nsw=350,
            #ispin=2,
            nelmin=4,
            ncore=24,
            nelm=70,
            #nelmdl=-10,
            ediffg=-0.05,
            isif=2,
            kpts = conv_kpt,
            gamma=True)


        if restart[ix]==0:
            #if site == 'hcp':
                #calc.set(algo='All')
            #else:
                #calc.set(algo='Normal')
            
            if site == 'hbn':
                interface = top.copy()
                interface.center(vacuum=15.0, axis=2)
                #calc.set(lreal=False)
            else:
                interface = bot_111.copy()
                #calc.set(lreal='Auto')

            
            if site != 'co-surf' and site != 'hbn':
                add_adsorbate(interface, top, int_dist, mol_index=m_ind, position=site)
                
                
            if site != 'hbn':
                
                if int_c:
                    add_adsorbate(interface, 'C', -1.05, position='fcc')
                    
                mask = [atom.tag > relax_n_layers for atom in interface]
                fixlayers = FixAtoms(mask=mask)
                interface.set_constraint(fixlayers)
            
            vp.write_vasp('temp.vasp', interface, label='test',direct=False,sort=True, vasp5=True)
            
            interf_final=vp.read_vasp('temp.vasp')
        
        else:
            fpath5= '{}/contcar_{}'.format(data_dir,site)
            interf_final=vp.read_vasp(fpath5)


        interf_final.set_calculator(calc)

        try: 
             energy=interf_final.get_potential_energy()
            
             fl.write('{:10s} {:10.4f}\n'.format(site, energy))
                       
             print(' {:10s}  {:10.4f}s'.format(site,energy))
            
        except ValueError as e:

             if 'invalid literal' in str(e):
                 fl.write('   {:10s} {:10s}\n'.format(site,'NaN'))
                 print('For {} forces are too high.'.format(site))
             else:
                 print(e)

        fname='{}/relaxed_{}.vasp'.format(data_dir,site) 
        fpath3 = '{}/vasp_{}.out'.format(data_dir,site)
        fpath4 = '{}/outcar_{}'.format(data_dir,site)
        fpath5= '{}/contcar_{}'.format(data_dir,site)

        fl.flush()
        
        vp.write_vasp(fname, interf_final, label='hBN on Co(111)',direct=False,sort=True, vasp5=True)
        
        rename('vasp.out', fpath3)
        rename('OUTCAR', fpath4)
        rename('CONTCAR', fpath5)

fl.close()

calc.clean()
