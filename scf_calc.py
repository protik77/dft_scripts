''' This script does SCF calculation for a given
structure.

Reads an input structure from the directory name
given as variable isif_dir
and then based on the input parameters, does SCF
calculation.

When the calculation is done, the output files
are stored in a directory name given as
scf_dir variable.

The files stored in the scf_dir are:
CHGCAR, OSZICAR, OUTCAR and vasp.out
'''


import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, makedirs, rename, remove
from shutil import *
import subprocess

conv_kpt=[8,8,4]

scf_dir='1.scf'

isif_dir='0.isif'

calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=0,
        sigma=0.1,
        lwave=False,
        lreal = False,
        encut = 400,
        algo='Normal',
        #kpar=6,
        nelm=100,
        #ispin=2,
        #isym=0,
        ediff=1e-8,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        kpts = conv_kpt,
        gamma=True)

print("\n-------------------------------------------")
print("SCF calculation of Heterostructure using ASE")
print("--------------------------------------------")

poscar_path=path.join(isif_dir,'CONTCAR')

l_scf_dir= path.join(scf_dir)

if not path.exists(l_scf_dir):
    makedirs(l_scf_dir)       

b=vp.read_vasp(poscar_path)

in2 = b.copy()

in2.set_calculator(calc)

energy = in2.get_potential_energy()

fpath1 = '{}/vasp.out'.format(l_scf_dir)

copyfile('vasp.out',fpath1)

fpath3 ='{}/OSZICAR'.format(l_scf_dir)

rename('OSZICAR',fpath3)

fpath4 ='{}/OUTCAR'.format(l_scf_dir)

rename('OUTCAR',fpath4)

fpath5 ='{}/CHGCAR'.format(l_scf_dir)

rename('CHGCAR',fpath5)

print('\n\n \t----- SCF Done -----\n')

