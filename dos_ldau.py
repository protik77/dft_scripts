''' This script calculates DOS with LDA+U for 
a given structure. Uses converged charge density
from the SCF step with SOC for the calculation.

Reads an input structure from the directory name
given as variable isif_dir,
creates symlink of the CHGCAR file from `sp_scf_dir`
and based on the input parameters, does DOS
calculation.

When the calculation is done, the output files
are stored in a directory dos<number_of_kpoints>.

The script read the Fermi level from
the OUTCAR of SCF directory which is
given as `soc_scf_dir`. The DOS calculation is done
from `below_fermi` to `over_fermi` with the `resolution`
given in meV.

The files stored in the dos<number_of_kpionts> are:
vasprun.xml, OUTCAR and vasp.out.
'''

import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, makedirs, rename, remove,symlink
import numpy as np
from shutil import *

# no of kpoints
kpoints=18

# initial moment
mom=5
def_mom=0
u_val=0.1

ldau_param={'Cr':{'L':2, 'U':u_val, 'J':0.0},
            'Sb':{'L':-1, 'U':0.0, 'J':0.0},
           }

# value of dos minimum and maximum energy in eV.
below_fermi=3.5
over_fermi=2.5

# resolution of the DOS in meV
resolution = 1

dos_kpt=[kpoints, kpoints, kpoints]

soc_scf_dir='1.sp.scf'

isif_dir='0.isif'

calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=-5,
        #sigma=0.01,
        lwave=False,
        lcharg=False,
        lreal = False,
        icharg=11,
        lorbit=11,
        ncore = 11,
        ispin=2,
        #nbands=70,
        encut = 400,
        algo='Normal',
        ldau=True,
        ldautype=2,
        ldau_luj=ldau_param,
        nelm=100,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=2,
        ediffg=-0.01,
        reciprocal=True,
        kpts = dos_kpt,
        gamma=True,
        )

print("\n------------------------")
print("DOS calculation using ASE")
print("-------------------------")

E_f=None
for line in open(path.join(soc_scf_dir,'OUTCAR'),'r'):
    if line.rfind('E-fermi') > -1:
        E_f = float(line.split()[2])

if E_f is None:
    raise ValueError('Fermi value not found')
else:
    print('\nFermi level is at {:2.2f}eV.'.format(E_f))


emin = E_f - below_fermi
emax = E_f + over_fermi
resolution = resolution/1000

nedos = int( ((emax-emin)/resolution) + 1)

print('Running on {} points from {:2.2f}eV to {:2.2f}eV.'.format(nedos,emin,emax))

poscar_path=path.join(isif_dir,'CONTCAR')

l_scf_dir= path.join(soc_scf_dir,'CHGCAR')

if path.islink('CHGCAR') or path.isfile('CHGCAR'):
    remove('CHGCAR')

symlink(l_scf_dir,'CHGCAR')

l_ek_dir='dos{}'.format(kpoints)

if not path.exists(l_ek_dir):
    makedirs(l_ek_dir)       

b=vp.read_vasp(poscar_path)

in2 = b.copy()

magmom=np.ones(len(in2))*def_mom
mn_ind=0
for idx, atom in enumerate(in2):

    if atom.symbol == 'Cr' and mn_ind==0 :
        magmom[idx] = mom
        mn_ind= mn_ind + 1
    elif atom.symbol == 'Cr' and mn_ind==1 :
        magmom[idx] = -mom
        break

calc.set(emin=emin,
         emax=emax,
         nedos=nedos,
         magmom=magmom,
    )

in2.set_calculator(calc)

energy = in2.get_potential_energy()

fpath2 ='{}/vasprun.xml'.format(l_ek_dir)

rename('vasprun.xml',fpath2)

fpath1 = '{}/vasp.out'.format(l_ek_dir)

copyfile('vasp.out',fpath1)

fpath4 ='{}/OUTCAR'.format(l_ek_dir)

rename('OUTCAR',fpath4)

fpath4 ='{}/POTCAR'.format(l_ek_dir)

rename('POTCAR',fpath4)

#calc.clean()
  
print('\n\n \t----- DOS Done -----\n')

