''' This script does SCF calculation with SOC for 
a given structure. Read the initial charge density
from the SCF step without SOC.

Reads an input structure from the directory name
given as variable isif_dir,
copies CHGCAR file from directory name given
as scf_dir variable
and based on the input parameters, does SCF
calculation.

When the calculation is done, the output files
are stored in a directory name given as
scf_dir variable.

Optionally it can process the LOCPOT file
to generate data for local potential plot.

The files stored in the scf_dir are:
CHGCAR, OSZICAR, OUTCAR and vasp.out
and optionally WAVECAR file which is
required for HSE calculations
'''

import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, makedirs, rename, remove
from shutil import *
import subprocess
import numpy as np

conv_kpt=[8,8,4]

# initial moment
mom=1

soc_scf_dir='3.soc.scf'

scf_dir='1.scf'

isif_dir='0.isif'

calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=0,
        sigma=0.1,
        lwave=False,
        lreal = False,
        #lvtot=True,
        encut = 400,
        algo='Normal',
        #ncore=12,
        nelm=150,
        #isym=0,
        lsorbit=True,
        ediff=1e-8,
        icharg=1,
        ibrion=-1,
        nsw=0,
        nelmin=2,
        ediffg=-0.01,
        kpts = conv_kpt,
        gamma=True)

print("\n-------------------------------------------")
print("SCF calculation of Heterostructure using ASE")
print("--------------------------------------------")

poscar_path=path.join(isif_dir,'CONTCAR')

l_scf_dir= path.join(soc_scf_dir)

chgcar_dir= path.join(scf_dir,'CHGCAR')

if path.islink('CHGCAR') or path.isfile('CHGCAR'):
    remove('CHGCAR')

copyfile(chgcar_dir,'CHGCAR')

if not path.exists(l_scf_dir):
    makedirs(l_scf_dir)       

b=vp.read_vasp(poscar_path)

in2 = b.copy()

magmom=np.ones(len(in2)*3)*mom

calc.set(magmom=magmom)

in2.set_calculator(calc)

energy = in2.get_potential_energy()

#locpot_path=path.join(l_scf_dir,'LOCPOT')

#move('LOCPOT',locpot_path)

#cmd=['vtotav',locpot_path,'Z']

#print('\n')

#subprocess.call(cmd)

fpath1 = '{}/vasp.out'.format(l_scf_dir)

copyfile('vasp.out',fpath1)

#fpath1 = '{}/WAVECAR'.format(l_scf_dir)

#copyfile('WAVECAR',fpath1)

fpath3 ='{}/OSZICAR'.format(l_scf_dir)

rename('OSZICAR',fpath3)

fpath4 ='{}/OUTCAR'.format(l_scf_dir)

rename('OUTCAR',fpath4)

fpath5 ='{}/CHGCAR'.format(l_scf_dir)

rename('CHGCAR',fpath5)

print('\n\n \t----- SOC-SCF Done -----\n')

