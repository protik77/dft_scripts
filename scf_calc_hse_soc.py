''' This script does SCF calculation with SOC and 
HSE for a given structure. Reads the wavefunctions 
from the SCF calculation with SOC.

Reads an input structure from the directory name
given as variable `isif_dir`,
copies `WAVECAR` file from directory name given
as `soc_scf_dir` variable
and based on the input parameters, does SCF
calculation.

When the calculation is done, the output files
are stored in a directory name given as
`hse_scf_dir` variable. Also stores `k-points` and `weights`
of the irreducible Brillouin zone in 
`ibzkp.txt` and `ibzkp_wt.txt` files, respectively.
This is used later for band structure calculation with HSE.

Optionally it can process the `LOCPOT` file
to generate data for local potential plot.

The files stored in the `hse_scf_dir` are:
`CHGCAR`, `OUTCAR`, `vasp.out` and `WAVECAR`
file which is required for HSE band structure
calculations.
'''

import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, makedirs, rename, remove
import subprocess
import numpy as np
from shutil import copyfile

# initial moment
mom=1

conv_kpt=[6,6,1]

hse_scf_dir='5.hse.scf'

soc_scf_dir='3.soc.scf'

isif_dir='0.isif'


calc = Vasp(prec = 'Accurate', 
        xc = 'hse06', 
        istart=1,
        ismear=1,
        sigma=0.1,
        lcharg=False,
        #lwave=False,
        lreal = False,
        #lvtot=True,
        encut = 400,
        kpar=2,
        #nelm=150,
        nelm=150,
        isym=0,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        lhfcalc=True,
        lsorbit=True,
        hfscreen=0.2,
        ialgo=53,
        algo='DAMPED',
        time=0.4,
        precfock='Normal',
        aexx=0.25,
        lmaxmix=4,
        nsim=4,
        kpts = conv_kpt,
        gamma=True)

print("\n-----------------------------------------------")
print("HSE-SCF calculation of Heterostructure using ASE")
print("------------------------------------------------")


hse_scf_dir_adr=path.join(hse_scf_dir)

relax_dir_adr=path.join(isif_dir)

scf_dir_adr=path.join(soc_scf_dir,'WAVECAR')

poscar_path=path.join(isif_dir,'CONTCAR')

het = vp.read_vasp(poscar_path)

copyfile(scf_dir_adr,'WAVECAR')

in2 = het.copy()

magmom=np.ones(len(in2)*3)*mom

calc.set(magmom=magmom)

in2.set_calculator(calc)

energy = in2.get_potential_energy()

ibzkp_wts=calc.get_k_point_weights()

ibzkp=calc.get_ibz_kpoints()

if not path.exists(hse_scf_dir_adr):
    makedirs(hse_scf_dir_adr)       

ibzkp_str=path.join(hse_scf_dir_adr,'ibzkp.txt')

ibzkp_wt_str=path.join(hse_scf_dir_adr,'ibzkp_wt.txt')

np.savetxt(ibzkp_str,ibzkp)

np.savetxt(ibzkp_wt_str,ibzkp_wts)

#rename('LOCPOT', scf_dir_adr)

#cmd=['vtotav',locpot_path,'Z']

#print('')

#subprocess.call(cmd)


fpath1 = '{}/vasp.out'.format(hse_scf_dir_adr)

rename('vasp.out',fpath1)

fpath4 ='{}/OUTCAR'.format(hse_scf_dir_adr)

rename('OUTCAR',fpath4)

fpath4 ='{}/WAVECAR'.format(hse_scf_dir_adr)

rename('WAVECAR',fpath4)

calc.clean()

print('\n\n \t----- Done -----\n')

