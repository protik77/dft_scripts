''' This script does nudged elastic band (NEB) calculation.

Reads the initial and final structure from `initial` and
`final`, creates `nimages` number of intermediate images
using `nebmake.pl` provided by VTST package and runs
NEB calculation from the given input parameters.

Uses the default NEB module of vasp.

Also creates some dummy files to avoid errors.
'''

from ase.calculators.vasp import Vasp
import ase.io.vasp as vp
from ase.io import *
import subprocess
from shutil import *
from os import path

# dummy structure ot run vasp
dummy = vp.read_vasp('start.vasp') 

# converged kpoints for the structure
conv_kpt=[2, 2, 1]

# number of images
nimages=7

# initial and final POSCAR name
initial='start.vasp'
final='end.vasp'

# summary file name
neb_summary_str='summary_neb.txt'

# vasp parameters
calc1 = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=1,
        sigma=0.1,
        lcharg=False,
        lwave=False,
        lreal = 'Auto',
        lorbit=0,
        images=nimages,
        lclimb=True,
        spring=-5,
        encut = 450,
        algo='Normal',
        nelm=80,
        #ncore=24,
        #kpar=4,
        ediff=1e-6,
        ibrion=3,
        potim=0,
        iopt=1,
        nsw=150,
        nelmin=4,
        ediffg=-0.05,
        kpts = conv_kpt,
        gamma=True)

print("\n------------------------")
print("NEB calculation using ASE")
print("-------------------------")

fl=open(neb_summary_str,'w')
fl.write('{:10s} {:12s} {:15s}\n'.format('Image', 'Energy (eV)','Barrier (eV)'))

cmd1=['nebmake.pl', initial, final, str(nimages)]
print('\n\n')

subprocess.call(cmd1)

copyfile(initial,'CONTCAR')
copyfile('outcar.start', 'OUTCAR')

fpath1 = path.join('00','OUTCAR')
copyfile('outcar.start',fpath1)

fpath2 = path.join('{:02d}'.format(nimages+1),'OUTCAR')
copyfile('outcar.end',fpath2)

dummy.set_calculator(calc1)
dummy.get_potential_energy()

for i in range(nimages+1):

    outcar_path = path.join('{:02d}'.format(i), 'OUTCAR')
    pot_en = read(outcar_path).get_potential_energy()

    if i == 0:
        pot_0 = pot_en

    fl.write(' {:3d} {:15.4f} {:12.3f}\n'.format(i, pot_en, pot_en-pot_0))

#calc1.clean()

fl.close()

print('---- NEB DONE ----')
