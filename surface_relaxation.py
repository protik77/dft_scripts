''' This script does surface relaxation on AFM
phase of CrSb using vasp

Reads an input structure from the given file name
and then based on the input parameters, runs
surface relaxation.

This script uses ISIF=2 tag of vasp for the relaxation
as it is faster.

The relaxed structure is stored in the directory
name given in isif_dir variable named as 'CONTCAR'.
This file is read for further calculations.

The relaxed structure is named as,
ase_<symbol>_vol_relaxed.vasp
'''

import ase.io.vasp as vp
import numpy as np
from ase.calculators.vasp import Vasp
from os import path, makedirs, rename, remove
from shutil import *

mat_poscar='NbSe2_1T_2L.vasp'

conv_kpt=[8,8,1]

isif_dir='0.isif'

calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=0,
        sigma=0.1,
        lcharg=False,
        lwave=False,
        lreal = False,
        encut = 400,
        algo='Normal',
        #ncore=12,
        nelm=100,
        #ispin=2,
        ivdw=1,
        ediff=1e-6,
        ibrion=1,
        nsw=150,
        nelmin=4,
        ediffg=1E-3,
        isif=2,
        kpts = conv_kpt,
        gamma=True)

print("\n--------------------------------------")
print("ISIF 2 Relaxation of material using ASE")
print("---------------------------------------")

b=vp.read_vasp(mat_poscar)

in2 = b.copy()

#in2 = in2 * (6,6,1)

#in2.center(vacuum=10, axis=2)

in2.set_calculator(calc)

energy = in2.get_potential_energy()

#fpath1 = path.join(isif_dir, 'BP_1L_relaxed.vasp')

#vp.write_vasp(fpath1, in2, vasp5=True, direct=False)

if not path.exists(isif_dir):
    makedirs(isif_dir)       

fpath1 = '{}/vasp.out'.format(isif_dir)

copyfile('vasp.out',fpath1)

fpath2 ='{}/CONTCAR'.format(isif_dir)

rename('CONTCAR',fpath2)

fpath3 ='{}/OSZICAR'.format(isif_dir)

rename('OSZICAR',fpath3)

fpath4 ='{}/OUTCAR'.format(isif_dir)

rename('OUTCAR',fpath4)

#fpath4 ='{}/POTCAR'.format(isif_dir)

#rename('POTCAR',fpath4)

#calc.clean()
   
print('\n\n \t----- ISIF 2 Done -----\n')

