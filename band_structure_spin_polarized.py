''' This script calculates spin polarized band structure with LDA+U for 
a given structure. Uses converged charge density
from the SCF step with SOC for the calculation.

Reads an input structure from the directory name
given as variable `isif_dir`,
creates symlink of the `CHGCAR` file from `sp_scf_dir`
and based on the input parameters, does band structure
calculation.

The path for the band structure calculation
is given by `ekpath_str` assuming the Brillouin
zone is hexagonal. `k-points` per line is given
by `kp_per_line`. The path for the band structure
is created accordingly.

When the calculation is done, the output files
are stored in a directory name given in `sp_ek_dir`.

The files stored in the soc_ek_dir are:
`vasprun.xml`, `OUTCAR` and `vasp.out`.
'''

import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from ase.io import read
from os import path, makedirs, rename, remove
from ase.dft.kpoints import get_special_points, bandpath
import numpy as np
from shutil import *
from os import *


kp_per_line=41

# initial moment
def_mom=0
mom=3.5
ekpath_str='GMKGALH'
u_val=0.25
u_val=0.0

ldau_param={'Cr':{'L':2, 'U':u_val, 'J':0.0},
            'Sb':{'L':-1, 'U':0.0, 'J':0.0},
           }

isif_dir='0.isif'

sp_scf_dir='1.sp.scf'

sp_ek_dir='2.sp.ek'

calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=0,
        sigma=0.05,
        lwave=False,
        lcharg=False,
        lreal = False,
        icharg=11,
        #nbands=50,
        lorbit=11,
        ispin=2,
        encut = 400,
        algo='Normal',
        #kpar=4,
        nelm=100,
        #ldau=True,
        #ldautype=2,
        #ldau_luj=ldau_param,
        #isym=0,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        reciprocal=True,
        )

print("\n------------------------------------")
print("E-k calculation of material using ASE")
print("-------------------------------------")



chgcar_path=path.join(sp_scf_dir,'CHGCAR')

poscar_path=path.join(isif_dir,'CONTCAR')

if path.islink('CHGCAR') or path.isfile('CHGCAR'):
    remove('CHGCAR')

symlink(chgcar_path,'CHGCAR')

b=vp.read_vasp(poscar_path)

if not path.exists(sp_ek_dir):
    makedirs(sp_ek_dir)       

in2 = b.copy()

points = get_special_points('hexagonal', in2.cell)
GMK = [points[k] for k in ekpath_str]

no_kpts=kp_per_line*len(GMK)

temp_kpts, x, X = bandpath(GMK, in2.cell, no_kpts)

del x
del X

final_shape = temp_kpts.shape

kpts=np.zeros((final_shape[0],4))

wt = np.ones((no_kpts,1), dtype=np.float64)

kpts[:,:-1]=temp_kpts
kpts[:,-1:] = wt

del temp_kpts
del wt

# set magnetic moments
magmom=np.ones(len(in2)) * def_mom
mn_ind=0
for idx, atom in enumerate(in2):

    if atom.symbol == 'Cr' and mn_ind==0 :
        magmom[idx] = mom
        mn_ind= mn_ind + 1
    elif atom.symbol == 'Cr' and mn_ind==1 :
        magmom[idx] = -mom
        break


calc.set(kpts=kpts,
         magmom=magmom,
        )

in2.set_calculator(calc)

energy = in2.get_potential_energy()

fpath1 = '{}/vasp.out'.format(sp_ek_dir)

copyfile('vasp.out',fpath1)

fpath2 ='{}/vasprun.xml'.format(sp_ek_dir)

copyfile('vasprun.xml',fpath2)

fpath4 ='{}/outcar.log'.format(sp_ek_dir)

copyfile('OUTCAR',fpath4)

print('\n\n \t----- SP-EK Done -----\n')

