''' This script restarts nudged elastic band (NEB) calculation.

In case the NEB calculation is not converged,
this script restarts the calculation.

The `nimages` needs to be the same as the initial
NEB calculation.

Also creates some dummy files to avoid errors.
'''

from ase.calculators.vasp import Vasp
import ase.io.vasp as vp
from ase.io import *
import subprocess
from shutil import *
from os import path

# dummy structure of run vasp
dummy = vp.read_vasp('start.vasp') 

# converged kpoints for the structure
conv_kpt=[2, 2, 1]

# number of images
nimages=6

# summary file name
neb_summary_str='summary_neb.txt'

# vasp parameters
calc1 = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=1,
        sigma=0.01,
        lcharg=False,
        lwave=False,
        lreal = 'Auto',
        lorbit=0,
        images=nimages,
        lclimb=True,
        spring=-5,
        encut = 550,
        algo='Normal',
        #nbands=120,
        nelm=80,
        #ncore=24,
        #kpar=4,
        ediff=1e-6,
        ibrion=3,
        potim=0,
        iopt=7,
        nsw=350,
        nelmin=4,
        ediffg=-0.01,
        kpts = conv_kpt,
        gamma=True)

print("\n------------------------")
print("NEB calculation using ASE")
print("-------------------------")

for i in range(1,nimages):

    this_dir='{:02d}'.format(i)

    contcar_str=path.join(this_dir, 'CONTCAR')
    poscar_str=path.join(this_dir, 'POSCAR')

    copyfile(contcar_str, poscar_str)
    copyfile(contcar_str, 'contcar.{}'.format(i))


fl=open(neb_summary_str,'w')
fl.write('{:10s} {:12s} {:15s}\n'.format('Image', 'Energy (eV)','Barrier (eV)'))

fpath1 = path.join('00','OUTCAR')
copyfile('outcar.start',fpath1)

fpath2 = path.join('{:02d}'.format(nimages+1),'OUTCAR')
copyfile('outcar.end',fpath2)

vp.write_vasp('CONTCAR',dummy, vasp5=True)
copyfile('outcar.start', 'OUTCAR')

dummy.set_calculator(calc1)
dummy.get_potential_energy()


for i in range(nimages+1):

    outcar_path = path.join('{:02d}'.format(i), 'OUTCAR')
    pot_en = read(outcar_path).get_potential_energy()

    if i == 0:
        pot_0 = pot_en

    fl.write(' {:3d} {:15.4f} {:12.3f}\n'.format(i, pot_en, pot_en-pot_0))

#calc1.clean()

fl.close()

print('---- NEB (restart) DONE ----')
