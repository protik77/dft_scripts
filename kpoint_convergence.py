''' This script checks k-point convergence using vasp.

This script reads an input structure as the variable ins_poscar,
runs k-point convergence for each of the
k-point given in the kps list and tabulates them
in a file name given by variable out_summary.

'''

import numpy
import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, rename, remove
from shutil import *

ins_poscar='NbSe2_bulk_2H.vasp'

ins= vp.read_vasp(ins_poscar)

out_summary='summary_kpts.txt'

if path.isfile(out_summary):
    op=open(out_summary, 'a')
else:
    op=open(out_summary, 'w')
    op.write('kpoints summary created using ase\n')
    op.write('kpoint \t energy \n')

kps = [4, 6, 8, 10, 12,14,16]
#kps = [4]

print("Calculation output")
print("------------------")
print('kpoint energy ')

calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=1,
        sigma=0.05,
        lcharg=False,
        lwave=False,
        lreal = False,
        encut = 400.0,
        algo='Normal',
        nelm=200,
        ncore=11,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        gamma=True)


# Generate cell configurations
for kp in kps:

  in2 = ins.copy()

  calc.set(kpts = [kp,kp,kp/2])

  in2.set_calculator(calc)
  
  energ=in2.get_potential_energy()

  op.write(' {:5d} {:8.4f} \n'.format(kp,energ))

  print(' {:8d} {:8.2f} '.format(kp,energ))

  fpath3 = 'vasp.kp.{:d}.out'.format(kp)
  
  if path.isfile(fpath3):
    remove(fpath3)

  copyfile('vasp.out', fpath3)


op.close()

#calc.clean()
       
print('\n\n Done')

