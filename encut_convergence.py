''' This script checks energy convergence using vasp.

This script reads an input structure as the variable ins_poscar,
runs energy convergence for each of the
encut given in the encuts list and tabulates them
in a file name given by variable out_summary.

'''

import numpy as np
import ase.io.vasp as vp
from ase.calculators.vasp import Vasp
from os import path, rename, remove
from shutil import *

ins_poscar='NbSe2_bulk_2H.vasp'

ins= vp.read_vasp(ins_poscar)

out_summary='summary_cutoff2.txt'

if path.isfile(out_summary):
    op=open(out_summary, 'a')
else:
    op=open(out_summary, 'w')
    op.write('encut file created using ase\n')
    op.write('encut \t energy \n')

conv_kpt=[8, 8, 4]

encuts = [200, 250,300, 350, 400, 450, 500]

print("Calculation output")
print("------------------")
print('encut  energy ')


calc = Vasp(prec = 'Accurate', 
        pp = 'pbe', 
        istart=0,
        ismear=1,
        sigma=0.1,
        lcharg=False,
        lwave=False,
        lreal = False,
        algo='Normal',
        nelm=200,
        #ncore=12,
        ediff=1e-6,
        ibrion=-1,
        nsw=0,
        nelmin=4,
        ediffg=-0.01,
        kpts = conv_kpt,
        gamma=True)


# Generate cell configurations
for e in encuts:

  in2 = ins.copy()

  calc.set(encut = e)

  in2.set_calculator(calc)
  
  energ=in2.get_potential_energy()

  op.write(' {:5d} {:5.6f} \n'.format(e,energ))

  print(' {:5d} {:5.4f} '.format(e,energ))

  fpath3 = 'vasp.encut.{:d}.out'.format(e)
  
  if path.isfile(fpath3):
    remove(fpath3)

  copyfile('vasp.out', fpath3)

op.close()

copyfile('POTCAR','potcar')

calc.clean()       


