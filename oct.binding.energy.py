''' This script does surface relaxation of B/N atom
on different sites of a metal 111 surface (i.e. Cu/Co/Ni)

This script puts an interstitial C atom on the octahedral site close to
the 111 surface then relaxes the B/N atom on different sites.

The `sup_size` variable defines the supercell size and the
`z_layers` defines the number of layers. For example `sup_size`
and `z_layers` as 4 and 4 denotes a surface supercell of
size 4x4x4. 

`int_dist` is the distance between the adsorotion atom
and the top of the surface.

`int_c` is a boolean flag to define whether the interstitial
C atom will be there or not.

`add_bn` defines the symbol of the adsorption atom.

`run_flag` defines calculation for which site is to be run.

`sites` list defines the adsorption sites of a 111 surface.

The final energies are tabulated in a file defined by 
`out_summary`. Functions `check_convergence`, `calc_binding_energy`
and `get_distance` found in `utility_functions.py` can be used
to extract binding energy of the sites.

All the relaxed structure along with vasp.out and OUTCAR
files are copied into `data-dir` with file name
<site_name>.<file_name>

'''

from ase.build import fcc111,add_adsorbate
from ase import Atoms as at
import ase.io.vasp as vp
from ase.constraints import FixAtoms, FixedPlane, FixedLine
import numpy as np
from os import path, makedirs, rename, remove, getcwd
from ase.calculators.vasp import Vasp
from shutil import *
from os import path, remove, rename


# input: how many layers of the surface in the z direction?
sup_size=4
z_layers=4
relax_n_layers=2     # how many layers to fix
m_ind=0              # index of the adsorbate atom to set
int_dist=1.5
int_c=False
add_bn='B'

sp_a = 3.5204 # lattice constant in Angstrom

conv_kpt=[6, 6, 1]


#run_flag=[1,1,1,1]
#run_flag=[1, 0, 0, 0]
#run_flag=[0, 1, 0, 0]
#run_flag=[1, 0, 0, 0]
run_flag=[1, 1, 1, 1]
sites=['ni-surf','fcc','hcp','ontop']

out_summary='bn_summary.txt'
data_dir='data'


print('\n------------------------------------------------------')
print(' Calculation of interlayer distance for binding energy')
print('                Written by: Protik Das                ')
print('------------------------------------------------------\n\n')

print(' Sites {}'.format(sites))
print(' Run flag {}'.format(run_flag))

if not path.exists(data_dir):
    makedirs(data_dir)       

if not path.isfile(out_summary):
    fl=open(out_summary,'w')
    fl.write('{:10s} {:10s}\n'.format('Config', 'Energy (eV)'))
else:
    fl=open(out_summary,'a')

bot_111 = fcc111('Ni', size=(sup_size,sup_size,z_layers), a=sp_a, vacuum=15.0) # create surface with a n X n X n supercell

bot_111.wrap()

cell = bot_111.get_cell()

for id,site in enumerate(sites):
    
    if run_flag[id]==1:

        print('\n\n Running for \'{}\' site.\n'.format(site))

        calc = Vasp(prec = 'Accurate', 
            pp = 'pbe', 
            istart=0,
            ismear=1,
            sigma=0.05,
            lcharg=False,
            lwave=False,
            lreal = 'Auto',
            encut = 550,
            #kpar=2,
            algo='Normal',
            #ncore=16,
            ediff=1e-6,
            ibrion=1,
            nsw=350,
            nelmin=4,
            nelm=80,
            ediffg=-0.01,
            isif=2,
            kpts = conv_kpt,
            gamma=True)

        interface = bot_111.copy()

        if site != 'ni-surf' :
            add_adsorbate(interface, add_bn, height=int_dist, position=site)
            
            
        if int_c:
            add_adsorbate(interface, 'C', -1.05, position='fcc')
            
        mask = [atom.tag > relax_n_layers for atom in interface]
        fixlayers = FixAtoms(mask=mask)
        interface.set_constraint(fixlayers)

        vp.write_vasp('temp1.vasp', interface, label='Ni (111) surface', direct=True, sort=True, vasp5=True)
        in2 = vp.read_vasp('temp1.vasp')

        in2.set_calculator(calc)

        energ1 = in2.get_potential_energy()

        fpath1 = path.join(data_dir,'{}.vasp.out'.format(site))
        fpath2 = path.join(data_dir, '{}.OUTCAR'.format(site)) 
        fpath3 = path.join(data_dir, '{}.CONTCAR.vasp'.format(site))

        copyfile('vasp.out', fpath1)
        rename('OUTCAR', fpath2)
        rename('CONTCAR',fpath3)

        fl.write(' {:9} {:9.4f}\n'.format(site, energ1))
        print(' {:9} {:9.2f}eV'.format(site,energ1))

        fl.flush()

        print('\n Finished running for {} site.'.format(site))

fl.close()

#calc.clean()

print(' done.')
